import time

from ovos_plugin_manager.phal import PHALPlugin
from ovos_utils.log import LOG
import RPi.GPIO as GPIO
from ovos_bus_client.message import Message
from ovos_utils.process_utils import RuntimeRequirements

# GPIO pins
BUTTON = 23
LED = 25

class GoogleAIYv1Validator:
    @staticmethod
    def validate(config=None):
        # Determine if Google AIY HAT v1 is connected
        AIYv1product = "Google AIY audioHAT Board v1"
        try:
            with open("/proc/device-tree/hat/product") as f:
                product = f.read().replace('\x00', '').strip()
                if product == AIYv1product:
                    return True
                else:
                    LOG.error(f'HAT detected but it was {product} instead of {AIYv1product} - skill will not load')
                    return False
        except:
            LOG.error(f'No HAT detected - skill will not load')
            return False


class AiyV1Plugin(PHALPlugin):

    validator = GoogleAIYv1Validator

    runtime_requirements = RuntimeRequirements(internet_before_load=False,
                                               network_before_load=False,
                                               requires_internet=False,
                                               requires_network=False,
                                               no_internet_fallback=True,
                                               no_network_fallback=True)

    def __init__(self, bus=None, config=None):
        super().__init__(bus=bus, name="ovos-PHAL-plugin-aiy-v1", config=config)
        try:
            GPIO.setmode(GPIO.BCM)
            GPIO.setwarnings(False)
            GPIO.setup(LED, GPIO.OUT)
            GPIO.setup(BUTTON, GPIO.IN, pull_up_down=GPIO.PUD_UP)
            GPIO.add_event_detect(BUTTON, GPIO.FALLING, bouncetime=500, callback=self.handle_button)
        except GPIO.error:
            self.log.warning("Can't initialize GPIO - skill will not load")

    def shutdown(self):
        super().shutdown()
        GPIO.cleanup()

    def on_record_begin(self, message=None):
        GPIO.output(LED, GPIO.HIGH)

    def on_record_end(self, message=None):
        GPIO.output(LED, GPIO.LOW)

    def handle_button(self, channel):
        GPIO.output(LED, GPIO.HIGH)
        self.bus.emit(Message("mycroft.mic.listen"))
